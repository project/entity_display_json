<?php

namespace Drupal\entity_display_json\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Entity Display JSON routes.
 */
class EntityDisplayJsonController extends ControllerBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The alias manager.
   *
   * @var Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * JsonController constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, AliasManagerInterface $aliasManager, Request $request) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->aliasManager = $aliasManager;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('path_alias.manager'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Get an array with all available translations of an entity and its path.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   An array with all available translations of the entity and its path.
   */
  public function getAvailableTranslations(EntityInterface $entity) {
    $availableTranslations = [];
    $languages = $entity->getTranslationLanguages();
    foreach ($languages as $id => $language) {
      $translation = $entity->getTranslation($id);
      $url = $translation->toUrl()->toString();
      $availableTranslations[$id] = $url;
    }
    return $availableTranslations;
  }

  /**
   * Processes an entity and prepares its data for JSON representation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object to be processed.
   * @param string $langcode
   *   The language code.
   * @param string $display_id
   *   (optional) The display ID. Defaults to 'default'.
   *
   * @return array
   *   An associative array representing the processed entity data.
   */
  public function processEntity(EntityInterface $entity, $langcode, $display_id = 'default') {
    $basePath = $this->request->getSchemeAndHttpHost();
    $entity_type = $entity->getEntityTypeId();
    if ($entity_type != 'view' && $entity->hasTranslation($langcode)) {
      $entity = $entity->getTranslation($langcode);
    }
    $access = $entity->access('view', NULL, TRUE);
    if (!$access->isAllowed() && $entity_type !== 'view') {
      return [];
    }
    $bundle = $entity->bundle();
    $data = [
      'entityType' => $entity_type,
      'bundle' => $bundle,
      'uuid' => $entity->uuid(),
      'id' => $entity->id(),
      'title' => $entity->label(),
    ];

    if ($entity_type == 'file') {
      $data['file_url'] = $entity->createFileUrl(FALSE);
      $data['file_name'] = $entity->getFilename();
      $data['file_size'] = $entity->getSize();
      $data['file_mime'] = $entity->getMimeType();
    }

    if ($entity->hasLinkTemplate('canonical')) {
      $path = $entity->toUrl()->getInternalPath();
      $data['path_alias'] = $basePath . $this->aliasManager->getAliasByPath('/' . $path);
    }
    // Views support.
    if ($entity_type == 'view') {
      $data['display_id'] = $display_id;
      $view = Views::getView($data['id']);
      if (is_object($view)) {
        $page = $this->request->query->get('page');
        $page ? $view->setCurrentPage($page) : NULL;
        $view->setDisplay($display_id);
        $display_handler = $view->getDisplay();
        $row_plugin = $display_handler->getPlugin('row');
        $results = $view->executeDisplay($display_id);
        $results_array = $results['#view']->result;
        if ($row_plugin->getPluginId() === 'entity:node') {
          $view_mode = $row_plugin->options['view_mode'];
          foreach ($results_array as $key => $result) {
            $results_array[$key] = self::processEntity($result->_entity, $langcode, $view_mode === 'full' ? 'default' : $view_mode);
          }
        }
        $data['results'] = $results_array;
        $filters = $view->display_handler->getOption('filters');
        $exposed_filters = array_filter($filters, function ($filter) {
          return !empty($filter['exposed']);
        });
        if (!empty($exposed_filters)) {
          $filtered_data = [];
          foreach ($exposed_filters as $key => $filter) {
            if (isset($filter['expose']['label'])) {
              $filtered_data[$key] = $filter['expose']['label'];
            }
          }
          $data['exposed_filters'] = $filtered_data;
        }
        $pager = $view->getPager();
        $data['pager'] = [
          'type' => $pager->getPluginId(),
          'items_per_page' => $pager->getItemsPerPage(),
        ];
        $data['total_rows'] = $results['#view']->total_rows;
      }
      return $data;
    }
    $display = $this->entityDisplayRepository->getViewDisplay($entity_type, $bundle, $display_id);
    $fields = $display->getComponents();

    foreach ($fields as $field_name => $field_value) {
      if (!isset($field_value['label'])) {
        continue;
      }

      // Entity reference fields.
      if ($entity->$field_name->target_id) {
        $ref_entities = $entity->$field_name->referencedEntities();
        $display_id = $field_value["settings"]["view_mode"] ?? NULL;
        foreach ($ref_entities as $key => $ref_entity) {
          if ($field_value["type"] == 'media_thumbnail') {
            $image_style = $field_value["settings"]["image_style"];
            $medias = $ref_entity->referencedEntities();
            foreach ($medias as $media) {
              if ($media->getEntityTypeId() == "file") {
                $imageUri = $media->getFileUri();
                $imgStyle = $this->entityTypeManager->getStorage('image_style')->load($image_style);
                $data[$field_name]['image_url'] = $imgStyle->buildUrl($imageUri);

              }
            }
            continue;
          }
          $data[$field_name][$key] = self::processEntity($ref_entity, $langcode, $display_id ?? $entity->$field_name->display_id);
          if (isset($ref_entity) && ($ref_entity->getEntityTypeId() == 'file') && ($ref_entity == $ref_entities[$key])) {
            $data[$field_name][$key]['uri'] = $ref_entity->getFileUri();
          }
        }
      }

      // Normal value fields.
      if ($value = $entity->$field_name->value) {
        // Summary and or trimmed.
        $summary = $entity->$field_name->summary;
        if (isset($field_value['settings']['trim_length'])) {
          $length = $field_value['settings']['trim_length'];
          $format = $entity->$field_name->format;
          $value = strip_tags($value);
          $trimmed = text_summary($value, $format, $length);
        }
        if ($field_value['type'] === 'text_summary_or_trimmed') {
          $value = $summary ?? $trimmed ?? $value;
        }
        if ($field_value['type'] === 'text_trimmed') {
          $value = $trimmed ?? $value;
        }
        $data[$field_name] = $value;
      }

      // Special fields.
      $field_type = $entity->get($field_name)->getFieldDefinition()->getType();

      // Links.
      if ($field_type === 'link_separate' || $field_type === 'link') {
        if (!$entity->$field_name->isEmpty()) {
          $values = $entity->$field_name->getValue();
          $links = [];
          foreach ($values as $value) {
            $url = $value['uri'];
            if (strpos($url, 'internal:') === 0) {
              $url = $basePath . '/' . str_replace('internal:', '', $url);
            }
            if (strpos($url, 'entity:') === 0) {
              $url = $basePath . '/' . str_replace('entity:', '', $url);
            }
            $publicUrl = $url;
            $link = ['url' => $publicUrl];
            // Since paragraphs don't have titles, let's skip them.
            if (!empty($value['title']) && $entity_type !== 'paragraph') {
              $link['title'] = $value['title'];
            }
            $links[] = $link;
          }
          $data[$field_name] = $links;
        }
      }

      // Add labels object except for entity references.
      if ($field_value['label'] == 'above' || $field_value['label'] == 'inline') {
        $data['labels'][$field_name]['display'] = $field_value['label'];
        $data['labels'][$field_name]['text'] = $entity->get($field_name)->getFieldDefinition()->getLabel();
      }

    }
    // Field groups support.
    $display_config = $display->toArray();
    if (isset($display_config['third_party_settings']['field_group'])) {
      $field_groups = $display_config['third_party_settings']['field_group'];
      foreach ($field_groups as $group_name => $group_config) {
        if (isset($group_config['children'])) {
          $specs = [
            'label' => $group_config['label'],
            'weight' => $group_config['weight'],
            'format_type' => $group_config['format_type'],
          ];
          empty($group_config["parent_name"]) ? $data[$group_name]['specs'] = $specs : $data[$group_config["parent_name"]][$group_name]['specs'] = $specs;
          foreach ($group_config['children'] as $field_name) {
            if ($data[$field_name]) {
              empty($group_config["parent_name"]) ? $data[$group_name][$field_name] ?? $data[$group_name][$field_name] = [] : $data[$group_config["parent_name"]][$group_name][$field_name] = $data[$field_name];
              unset($data[$field_name]);
            }
          }
        }
      }
      // Mode labels to the end.
      if (isset($data['labels'])) {
        $labels = $data['labels'];
        unset($data['labels']);
        $data['labels'] = $labels;
      }
    }

    return $data;
  }

  /**
   * Generates the JSON response based on entity display settings.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $uuid
   *   The UUID of the entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $display_id
   *   (optional) The display ID. Defaults to 'default'.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the entity or display cannot be found.
   */
  public function build($entity_type, $uuid, Request $request, $display_id = 'default') {
    $entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(['uuid' => $uuid]);
    $entity = reset($entities);
    $langcode = $request->query->get('lang', 'default');
    if (!$entity) {
      throw new NotFoundHttpException(sprintf('Entity with UUID %s not found.', $uuid));
    }
    $data = $this->processEntity($entity, $langcode, $display_id);
    // Preprend apiVersion $data.
    $data = [
      'apiVersion' => '1.0',
      'langcode' => $entity->language()->getId(),
      'translations' => $entity_type != 'view' ? $this->getAvailableTranslations($entity) : [],
    ] + $data;
    // Return the JSON response.
    return new JsonResponse($data);
  }

}
