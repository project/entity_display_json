## Features

### Site settings

The route '/ejson' provides basic site info:
- Site name
- Site slogan
- Default language
- Languages (each with langcode, labels and path)

### Retrieving content

Access content in JSON format through the route /ejson/{entity_type}/{uuid}/{display_id}. This endpoint:

- Handles References: Automatically iterates through all referenced entities to compile comprehensive content data, ensuring a complete representation of the requested entity.
- Optional Display ID: The {display_id} parameter is optional. If omitted, 'default' is used, allowing flexibility in specifying display modes for entities.
- Entity Display Customization: Leverage entity display settings to control which fields are visible. This is especially useful for tailoring the JSON output, including customizing image URLs for thumbnails and other media.

*Language Specific Content:*

To retrieve content in a specific language, append a query parameter like ?lang=en to the URL. This feature enables on-the-fly language customization for your content, making the module adaptable for multilingual sites.